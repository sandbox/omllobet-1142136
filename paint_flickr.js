// $Id$
/**
 * Script helpers for paint - flickr integration.
 */ 

Drupal.behaviors.paint = function (context) {
  if(Drupal.jsEnabled) {
    $(window).load( function() {
      $('#flickr-container').removeClass('loading');
      $('#flickr-visible').removeClass('invisible');
      $('.flickr-photo').fadeIn();
    });
    $('.flickr-photo').click( function(){
      $('#flickr-container').addClass('loading');
      $('.flickr-photo').fadeOut();
      var flickrUrl = $(this).attr('id');
      var flickrTitle = $(this).find("img").attr('alt');
      var container = flickrUrl.split('/');
      document.forms['paint-flickr-download-form'].title.value = flickrTitle;
      document.forms['paint-flickr-download-form'].serverid.value = container[3];
      var lastPart = container[4].split('_');
      document.forms['paint-flickr-download-form'].id.value = lastPart[0];
      document.forms['paint-flickr-download-form'].secret.value = lastPart[1];
      document.forms['paint-flickr-download-form'].fid.value = container[2].replace(/\D/g,'');
      document.forms['paint-flickr-download-form'].submit();
      return false;
    });
  }
}