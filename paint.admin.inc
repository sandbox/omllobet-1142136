<?php
// $Id$

/**
 * @file
 * paint admin settings
 */

/**
 * Paint settings form.
 */
function paint_admin_form() {
  $form = array();
  $form['paint_typename'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of your image content type'),
    '#default_value' => variable_get('paint_typename', 'image'),
    '#size' => 15,
    '#maxlength' => 15,
    '#description' => t("The name of the image content type created."),
    '#required' => TRUE,
  );
  $form['paint_imagefieldname'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of your image content type field'),
    '#default_value' => variable_get('paint_imagefield_name', 'image'),
    '#size' => 15,
    '#maxlength' => 15,
    '#description' => t("The name of the image field created."),
    '#required' => TRUE,
  );
  $form['paint_paintweb_config'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of the paintweb configuration file'),
    '#default_value' => variable_get('paint_paintweb_config', 'config-example.json'),
    '#size' => 20,
    '#maxlength' => 20,
    '#description' => t("The name of the paintweb configuration file."),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Paint settings form validation.
 */
function paint_admin_form_validate($form, &$form_state) {
  $field_name = $form_state['values']['paint_imagefieldname'];
  if (strcmp($field_name, check_plain($field_name)) != 0) {
    form_set_error('paint_imagefieldname', t('The name of the field is not safe to save.'));
  }
  $typename = $form_state['values']['paint_typename'];
  if (strcmp($typename, check_plain($typename)) != 0) {
    form_set_error('paint_typename', t('The name of the type is not safe to save.'));
  }
  $typename = $form_state['values']['paint_paintweb_config'];
  if (strcmp($typename, check_plain($typename)) != 0) {
    form_set_error('paint_paintweb_config', t('The name of the configuration file is not safe to save.'));
  }
}