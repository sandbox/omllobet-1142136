<?php
// $Id$

/**
 * @file
 * flickr integration
 */

/**
 * Flickr form to set the search text.
 */
function paint_flickr_form() {
  $form = array();
  $form['searchtext'] = array(
    '#type' => 'textfield',
    '#title' => t('Text'),
    '#default_value' => t('Text to search'),
    '#size' => 60,
    '#maxlength' => 64,
    '#required' => TRUE,
    '#attributes' => array(
      'onFocus' => "if (this.value=='" . t('Text to search') . "') this.value='';"
     )
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  return $form;
}

/**
 * Redirection of a search after submitting the search form.
 */
function paint_flickr_form_submit($form, &$form_state) {
  $text = $form_state['values']['searchtext'];
  if (arg(0) != 'noderef_image_helper') {
    $form_state['redirect'] = 'paint/flickr/'. check_plain($text);
  }
  else {
    $form_state['redirect'] = 'noderef_image_helper/flickr/' . arg(2) . '/' . check_plain($text);
  }
}

// Flickr
/**
 * Flickr integration page.
 */
function paint_flickr() {
  if (!module_exists('flickrapi')) {
    drupal_set_message(t('Flickr integration needs Flickr API module installed and enabled.'));
  };
  return drupal_get_form('paint_flickr_form');
}

/**
 * Gets photos from flickr
 */
function paint_get_flickr_photos($text, $id = NULL) {
  $flickr = flickrapi_phpFlickr();
  $text_title = t('Pictures of: @text', array('@text' => $text));
  $ret = theme('paint_flickr_title', $text_title);
  drupal_add_css(drupal_get_path('module', 'paint') .'/paint.css');
  // Search by the text//license =7, No known copyright restrictions.
  $photos = $flickr->photos_search(array("text" => $text, "sort" => "interestingness-desc", "per_page" => 12, "safe_search" => 1, "license" => "2,3,4,5,6,7"));
  if ($photos['total'] == 0) {
    return t("We couldn't find any photos that match your criteria");
  }
  $ret .= drupal_get_form('paint_flickr_download_form', $id) . theme('paint_flickr_photos', $photos, $flickr);
  return $ret;
}

/**
 * Form with the fields to be able to download an image form flickr.
 */
function paint_flickr_download_form($form_state, $id = NULL) {
  $form = array();
  $form['fid'] = array(
    '#type' => 'hidden',
    );
  $form['serverid'] = array(
    '#type' => 'hidden',
    );
  $form['id'] = array(
    '#type' => 'hidden',
    );
  $form['secret'] = array(
    '#type' => 'hidden',
    );
  $form['title'] = array(
    '#type' => 'hidden',
    );
  if (arg(0) != 'noderef_image_helper' || $id == NULL) {
    $form['#action'] = url('paint/flickr/download');
  }
  else {
    $form['#action'] = url('noderef_image_helper/download/' . $id);
  }

  $form['#submit'] = array(
    '#type' => 'submit'
  );
  return $form;
}

/**
 * Downloads an image from flickr and creates an anonymous image node from it.
 */
function paint_download_image($form_state, $elem_id = NULL) {
  $farm_id = (int)$form_state['post']['fid'];
  $server_id = (int)$form_state['post']['serverid'];
  $id = $form_state['post']['id'];
  $secret = $form_state['post']['secret'];
  $title = $form_state['post']['title'];
  // Check fields.
  if (!is_int($farm_id) || !is_numeric($server_id) || !is_numeric($id) ||
      !isset($farm_id) || !isset($server_id) || !isset($id)) {
    return drupal_access_denied();
  }
  $url = 'http://farm' . check_plain($farm_id) . '.static.flickr.com/' . check_plain($server_id) . '/' . check_plain($id) . '_' . check_plain($secret);
  $result = drupal_http_request($url);

  if (!isset($result->error)) {
    //generate file
    $filename = hash('sha256', $url) . '.jpg';
    $filepath = file_directory_path() . '/images';
    //if the directory doesn't exist file_save_data will fail
    if (!is_dir($filepath)) {
      mkdir($filepath);
    }
    $file =$filepath . '/' . $filename;
    while(file_exists($file)) {
      $file= $filepath . '/' . random(18) . $filename;
    }
    //save
    if (file_save_data($result->data, $file , FILE_EXISTS_REPLACE) === 0) {
      drupal_set_message(t("We couldn't save the image."));
    }
    $mime = file_get_mimetype($file);
    if (empty($title)) {
        $title = t('No title');
    }
    $file = basename($file);
    global $user;
    $tags = '';
    $flickr = flickrapi_phpFlickr();
    $info = $flickr->photos_getInfo($id);
    foreach($info['tags']['tag'] as $tag) {
      $tags = $tags .  $tag['raw'] . ",";
    }
    //dpm($info);
    $license = $info['license'];
    $description = strip_tags($info['description']);
    $author = $info['owner']['realname'];
    $ret =_paint_create_node( $title, $license, $author, $description, $tags , _paint_register_file($file, $mime, file_directory_path() . '/images/' . $file, $user->uid));
    if ($ret == -1) {
      drupal_set_message(t('Image creation failed'));
      drupal_goto('paint');
    }
    else {
      if (arg(0) != 'noderef_image_helper') {
        drupal_goto('node/' . $ret);
      }
      else {
        drupal_goto('noderef_image_helper/insert/' . $elem_id . '/' .  $ret);
      }
    }
  }
  else {
    drupal_set_message(t('Downloading the file failed.'));
    drupal_goto('paint');
  }
}

/**
 * Menu callback for import popup.
 */
function paint_noderef_image_helper_import($id) {
  global $user;

  if (module_exists('admin_menu')) {
    admin_menu_suppress();
  }
  module_load_include('inc', 'paint', 'paint.flickr');
  $content = '<h1>' . t('Import Image') . '</h1>';
  $content .= drupal_get_form('paint_flickr_form');
  echo theme('noderef_image_helper_popup', $content);
  exit;
}

/**
 * Menu callback for show photos in popup.
 */
function paint_noderef_image_helper_flickr($id, $text) {
  global $user;

  if (module_exists('admin_menu')) {
    admin_menu_suppress();
  }
  module_load_include('inc', 'paint', 'paint.flickr');
  $content = '<h1>' . t('Import Image') . '</h1>';
  $content .= paint_get_flickr_photos($text, $id);
  echo theme('noderef_image_helper_popup', $content);
  exit;
}

/**
 * Theme function for theming title.
 */
function theme_paint_flickr_title($text) {
  return '<em>' . $text . '</em>';
}

/**
 * Theme function for theming flickr photos.
 */
function theme_paint_flickr_photos($photos, $flickr) {
  $output = '<div id="flickr-container" class="loading"><br /><div id="flickr-visible" class="invisible"><ol>';
  foreach ((array)$photos['photo'] as $photo) {
    // Build image and link tags for each photo.
    $img = theme_image(check_url($flickr->buildPhotoURL($photo, "Thumbnail")), $photo['title'], $photo['title'], array('border' => 0), FALSE);
    $output .= '<li style="display: inline;">' . l($img, 'http://www.flickr.com/photos/' . check_plain($photo['owner']) . '/' . check_plain( $photo['id']), array('html' => 'true', 'attributes' => array('id' => check_url($flickr->buildPhotoURL($photo, "Medium")), 'class' => 'flickr-photo'))) . '</li>';
  }
  $output .= "</ol></div></div>\n";
  drupal_add_js(drupal_get_path('module', 'paint') . '/paint_flickr.js');
  return $output;
}