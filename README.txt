// $Id$

-- SUMMARY --

The paint module integrates paintweb http://code.google.com/p/paintweb/ as a
standalone application into drupal. So it let's you create and edit
images in Image nodes. It also allows you to import images from
Flickr and edit them.

-- REQUIREMENTS --

* Paintweb http://code.google.com/p/paintweb/
  It is not a module, just download and place it under sites/all/modules/paint/paintweb
  So the build directory of paintweb must be on sites/all/modules/paint/paintweb/build

* Imagefield http://drupal.org/project/imagefield

 Optional requirements (to import Flickr images)

  * FlickrApi http://drupal.org/project/flickrapi
    Please, do not forget to fill flickr api

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.
* Paintweb folder must be at sites/all/modules/paint/paintweb
* Imagefield: Install as usual

-- CONFIGURATION --

Please, carefully read this section:

* Configure user permissions in Administer >> User management >> Permissions

* If you have installed flickrapi, set flickr api key

* Configure image node: You have to create an image content type http://drupal.org/node/609628 Step 2
  I recommend to use 'image' as node type and field_'image' as field name (without single quotes)

* Configure paintweb settings in Administer >> Site Configuration >> Paint settings
  If you don't use a image node type with a field_image field, you can change settings here.
 
-- CONTACT --

Current maintainers:
* Oscar Martinez - http://drupal.org/user/871492

-- TODO --
*look at libraries module
*cck widget?
*simplify permissions
*remove inline javascript
