<?php
// $Id$

/**
 * @file
 * paint node copy
 */

/**
 * Image node copy function.
 */
function paint_node_copy($nid) {
  if (is_numeric($nid)) {
    global $user;
    $node = node_load($nid);
    $node_type = variable_get('paint_typename', 'image');
    if ( $node->status == 1 && ((user_access('copy any ' . $node_type . ' content') || ($user->uid == $node->uid && user_access('copy own ' . $node_type . ' content'))))) {
      $node->nid = NULL;
      $node->uid = $user->uid;
      $node->name = $user->name;
      $node->created = 0;
      $node = node_submit($node);
      node_save($node);
    }
    else {
      drupal_set_message(t("You don't have permissions for copying an image"), 'error');
    }
  }
  drupal_goto('paint/'. $node->nid);
}
