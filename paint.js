// $Id$
/**
 * Script helpers for paint.
 */ 

Drupal.behaviors.paint = function (context) {
  if(Drupal.jsEnabled) {
    // If javascript performance option && clean urls are enabled, this line is needed.
    PaintWeb.baseFolder = Drupal.settings.paint.paintWebPath;
    // Paintweb main object.
    paintPw = new PaintWeb();
    var paintTarget = document.getElementById('PaintWebTarget');
    paintPw.config.guiPlaceholder = paintTarget;
    
    paintPw.config.configFile = Drupal.settings.paint.config;
    paintPw.config.lang = Drupal.settings.paint.language; // Language integration.
    //without this chrome seem to load the image too soon and sometimes we get a blank image
    $(document).ready(function() {
      var paintImg  = document.getElementById('PaintEditableImage');
      if (paintImg) {
        paintImg.style.display = 'none';
        paintPw.config.imageLoad = paintImg;
      }
      paintPw.init( paintInitStatus );
    //Save Image integration.
      var evId = paintPw.events.add('imageSave', paintSaveHandler);
    });
  }

  function paintInitStatus (ev) {
    if (ev.state === PaintWeb.INIT_DONE) {
      $('.paint-status').text(Drupal.t('PaintWeb successfully loaded!'));
    } else if (ev.state === PaintWeb.INIT_ERROR) {
      $('.paint-status').text(Drupal.t('PaintWeb loading failed!'));
    }
    $('#PaintEditableImage').removeClass('loading');
    setTimeout("$('.paint-status').css('visibility', 'hidden');",4000);
  }
};

// Manually triggers the imageSave event
function paintManuallySave() {
    paintPw.imageSave();
}

// The imageSave event handler.
function paintSaveHandler (ev) {
  // Cancel the default action of the event
    ev.preventDefault();
  // Get variables from the form.
  var title = $('input#edit-title').val();
  $('.paint-status').text(Drupal.t('Saving...'));
  $('.paint-status').css('visibility', 'visible');
  var nid = "nid=0&";
  if (Drupal.settings.paint.nid != null) {
    nid = "nid=" + Drupal.settings.paint.nid + "&";
  }
  $.ajax({
    type: "POST",
    url: "?q=paint/save",
    data: nid + "title=" + title + "&dataURL=" + encodeURIComponent(ev.dataURL) + "&form_token=" + Drupal.settings.paint.token,
    complete: function (xhr, textStatus) {
      // Check if its in the correct state.
      if (!xhr || xhr.readyState !== 4) {
        return;
      }
      if (xhr.responseText) {
        status = xhr.responseText.substring(0,2);
      }
      if ((xhr.status !== 304 && xhr.status !== 200) ||
      !xhr.responseText || status !== 'OK') {
        // Image save failed.
        $('.paint-status').text(Drupal.t('Image saving failed'));
      }
      else {
        // Image save was successful.
        $('.paint-status').text(Drupal.t('Image successfully saved'));
        new_nid = xhr.responseText.substring(xhr.responseText.indexOf(':')+1);
        window.location = "?q=node/" + new_nid;
      }
      setTimeout("$('.paint-status').css('visibility', 'hidden');",4000);
    }
  });
};