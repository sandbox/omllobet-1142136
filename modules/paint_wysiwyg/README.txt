
To install:

1-install http://drupal.org/project/wysiwyg and tinymce

2-copy contents of sites/all/modules/paint/paintweb/ext/tinymce-plugin
to /sites/all/libraries/tinymce/jscripts/tiny_mce/plugins

3.- Go to web /admin/settings/wysiwyg
and activate the Paintweb edit plugin

That's it.


FAQ

Q:it doen't work, it doesn't pass the paintweb is loading stage, what do I do?

A:
if it does not work go to /sites/all/libraries/tinymce/jscripts/tiny_mce/plugins/paintweb/editor_plugin.js and editor_plugin.js
and change:

in editor_plugin.js
in line 1:
 tinymce.ScriptLoader.load(p,N)
to
 tinymce.ScriptLoader.add(p,N);
 tinymce.ScriptLoader.loadQueue()

and in editor_plugin_src.js

line 148:
  tinymce.ScriptLoader.load(src, paintwebLoaded);
to
  tinymce.ScriptLoader.add(src, paintwebLoaded);
  tinymce.ScriptLoader.loadQueue();


Q: Images are not displayed in output

A: Add <img> to the HTML filter (Administer -> Site Configuration -> Input Formats) to allow the tag.